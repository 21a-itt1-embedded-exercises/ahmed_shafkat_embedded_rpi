import machine #imports machine library
import utime   #imports utime library

led1 = machine.Pin(14, machine.Pin.OUT)  #assign GP14 as external led1 output
led2 = machine.Pin(15, machine.Pin.OUT)  #assign GP15 as external led2 output
btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)  #assign GP18 as external btn1 input 
btn2 = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP)  #assign GP19 as external btn2 input
led_onboard = machine.Pin(25, machine.Pin.OUT)   #assign GP25 as onboard LED output 

while True:                   #continues the loop infinitely
    if btn1.value() == 0:     #checks if btn1 is pressed
        led1.toggle()         #turns the external led1 on from off state
        utime.sleep(0.3)      #pauses the program for 0.3 seconds
        led_onboard.toggle()  #turns the led_onboard on from off state
        utime.sleep(0.3)      #pauses the program for 0.3 seconds
    else:                         # executes if the above condition is false
        if btn2.value() == 0:     #checks if btn2 is pressed
            led2.toggle()         #turns the external led2 on from off state
            utime.sleep(0.40)     #pauses the program for 0.4 seconds
            for i in range(2):        #to repeat the following action 2 times
                led_onboard.toggle()  #turns the led_onboard on from off state
                utime.sleep(0.10)     #pauses the program for 0.1 seconds
            
            