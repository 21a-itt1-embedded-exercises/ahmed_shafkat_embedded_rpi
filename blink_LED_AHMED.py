import machine    # in order to implement instractions we can import library from python repository.
import utime      # utime as like the machine library but this will work with time
led_onboard = machine.Pin(25, machine.Pin.OUT)  # led_onboard is the variable name. 

while True:
    led_onboard.value(1)  # value(1) means, turn on the led.
    utime.sleep(1)        # this function let led stay on for () of second. 
    led_onboard.value(0)  # turn off the led
    utime.sleep(0.5)      # led will stay turned off for () second. 
    
    